from prov_agent.deployer import Deployer
from prov_agent.module import Module


class Agent(object):
    def __init__(self):
        self.deployer = Deployer.get_provider()

    def process_msg(self, msg):
        print "Processing Message - %s" % msg
        resource = msg.pop('resource', None)
        if resource == 'module':
            return self.process_module(msg)

    def process_module(self, msg):
        oper = msg.pop('oper', None)
        name = msg.pop('name', None)
        url = msg.pop('url', None)
        repo = msg.pop('repo', None)
        ref = msg.pop('ref', 'master')
        module_kwargs = {}
        properties = msg.pop('properties', None)
        mod = Module.modules.get(name, None)
        if not mod:
            mod = Module(name, url, repo, ref, self.deployer.module_config)

        if properties:
            module_kwargs.update({'properties': properties})

        provider = mod.provider
        try:
            return provider.serializable(getattr(provider, oper)(**module_kwargs))
        except Exception as e:
            print e
            raise


def main():
    import zmq
    agent = Agent()
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    #socket.bind("tcp://*:5000")
    socket.bind("ipc:///tmp/filename")
    print "Ready to get messages on ipc "
    while True:
        try:
            msg = socket.recv_json()
            print "Got", msg
            response = agent.process_msg(msg)
            try:
                socket.send_json(response)
            except Exception as e:
                print e
                raise
        except Exception as e:
            print "Exception: %s" % e
            raise


if __name__ == '__main__':
    main()
